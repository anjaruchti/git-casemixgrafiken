#Make Plots
#ANPASSEN!! sowie im Sourcefile Pfad von i18n file anpassen!

library(data.table)
library(ggplot2)
library(readr)
library(dplyr)
library(scales)
library(extrafont)
library(shiny.i18n)
#Anja
setwd("C:/Users/cscha/Desktop/projekt r grafiken nat.vergleichsbericht/grafiken")

#Christian


#output folder (wo sollen die png file hin) uncomment und change next line falls der Outpufolder noch erstellt werden muss
#dir.create("~/w2/Vergleichsberichte/all_images_EP/italienisch")

#setwd("C:/Users/dev/Desktop/grafiken-nat-vergleichsbericht/ep_plot_test/new4")

# setwd("~/w2/Vergleichsberichte/all_images_EP/italienisch")

#Data loading
data_file <- file.choose()

#achtung bei guess_max! so hoch wie möglich setzen, damit column type richtig erkannt wird bei columns
#mit vielen NA am Anfang
data_all <- read_csv(data_file, guess_max = 40000)

######### Für FP Daten, Daten anpassen: Variable "kliniktyp" hinzufügen, typ 4 bspw., für klinik_anonym in Text umwandeln; 01, 02...
data_all$klinik_anonym <- ifelse(data_all$klinik_anonym == '1','01', data_all$klinik_anonym)
data_all$klinik_anonym <- ifelse(data_all$klinik_anonym == '2','02', data_all$klinik_anonym)
data_all$klinik_anonym <- ifelse(data_all$klinik_anonym == '3','03', data_all$klinik_anonym)
data_all$klinik_anonym <- ifelse(data_all$klinik_anonym == '4','04', data_all$klinik_anonym)
data_all$klinik_anonym <- ifelse(data_all$klinik_anonym == '5','05', data_all$klinik_anonym)
data_all$klinik_anonym <- ifelse(data_all$klinik_anonym == '6','06', data_all$klinik_anonym)
data_all$klinik_anonym <- ifelse(data_all$klinik_anonym == '7','07', data_all$klinik_anonym)


data_all$kliniktyp <- rep(4, nrow(data_all))
##### 

# Funktionen einlesen, subsets erstellen
source_file <- file.choose()
#source("C:/Users/cscha/Desktop/projekt r grafiken nat.vergleichsbericht/Franz/Franz/Sourcefile_EP_Vergleichsbericht_V4.R")
source(source_file, encoding = "utf-8")

# Wähle typ (1:= Akut- und Grund, 2:= Schwerpunkt, 3:= Abhängigkeit )
typ <- 4
# Wähle Eerhebungszeitraum (Werte = 0 bis 5, kodierung siehe ANQ Codebuch)
zeit <- 2

#Wähle Sprache
#Translation ---- 
translation_file <- file.choose()

#i18n <- Translator$new(translation_json_path = "/Users/cscha/Desktop/projekt r grafiken nat.vergleichsbericht/Franz/Franz/translations_copy_vergleichsbericht_V2.json")
i18n <- Translator$new(translation_json_path = translation_file)
#Auswahl Sprache(1 = Deutsch, 2 = Franzoesisch, 3 = Italienisch)
sprache <- i18n$languages[1]
i18n$set_translation_language(sprache)


#k_controll ----
# k_cotrol this is a function that should produce a vector containing all Klinik_nr_verschluesselt values 
# for clinics of each kind EP, KJP or FP this code should work for data from 2017 but for it to work with 
# data from 2018 onwards there will have to be made changes

# 1.set path to the ruecklaufkontrolle csv file from the correct year for your report
# 
# rk <- file.choose()
# 
# ##Kliniken der Akut- und Grundversorgung (AkuGruVer)
# AkuGruVer     <- c("03","06","07","08","12","13","17","18","21","22","24","26","28","30","32","34","35","36",
#                    "37","39","40","42","43","44","46","48","49","51","52","54","56","58","66", "71")
# 
# ##Kliniken der Schwerpunktversorgung (SchwerpuVer)   !!!Ohne die Klinik Gais (Kt. AR)!!
# SchwerpuVer   <- c("01","02","09","10","14","15","19","23","25","27","29","33","38","45","47","50","57","59",
#                    "60","61","62","63","65","67", "69")
# 
# ##Kliniken fuer Abhaengigkeitserkrankgungen (Abhaengigkeit)   
# Abhaengigkeit <- c("04","05","11","16","20","31","53","55","68") 
# 
# epSubChoice <- list(AkuGruVer, SchwerpuVer, Abhaengigkeit)
# 
# k_control <- function(rk, isEp = FALSE, eptype = 0){
#   
#   if(isEp){
#     all_k <- epSubChoice[[eptype]]
#     return(all_k)
#   }
#   else if(isEp == FALSE){
#     k_list <- read_csv2(rk)
#     k_list <- dplyr::select(k_list, Klinik_nr_verschluesselt)
#     all_k <- k_list$Klinik_nr_verschluesselt
#     all_k <- all_k[!is.na(all_k)]
#     return(all_k)
#   }
# }
# 
# k_control_list <- k_control(rk = rk, isEp = TRUE, eptype = 2)
# #recoding im Sourcefile


#Ausgabe der Missing value information
missing <- data.frame("plot" = character(0), "wert" = character(0), stringsAsFactors = FALSE)

#typus <- eval(parse(text = paste0("Kliniken", typ)))

#Abbildung 5.1.1.----
#Limit parameter = Anpassen Y-Achsen bereich , bei 0 -> Achsen max. dynamisch 10 % gröesser als maximal Wert. Ansonsten direkt Wert (Bsp. 750) eingeben)
abgesch_faelle(data_all, zeit, typ, limit = 0)

#Abbildung 5.1.2.----
alter_eintritt(data_all, zeit, typ)

#Abbildung 5.1.3. ----
geschlecht_plot(data_all, zeit,typ) 

#Abbildung 5.1.4.----
#y-scale (10er Schritte bei default)
verteilung_hauptdiagnosen(data_all, zeit, typ, yscale = 10)

#Abbildung 5.1.5.----
honos_bscl(data_all, zeit, typ)

#Abbildung 5.1.6.----
FU_plot(data_all, zeit, typ)

#Abbildung 5.1.7----
national_plot(data_all, zeit, typ)

#Abbildung 5.1.8.----
verteilung_bildung(data_all, zeit, typ, yscale = 10)

#Abbildung 5.1.9.----
zivilstand(data_all, zeit, typ, yscale = 10)

#Abbildung 5.1.10.----
beschaftigung_vor_Eintritt(data_all, zeit, typ, yscale = 10)

#Abbildung 5.1.11.----
aufenthaltsort(data_all, zeit, typ, yscale = 10) 

#Abbildung 5.1.12.----
behandlungsklasse(data_all, zeit, typ, yscale = 20)

#Abbildung 5.1.13.----
einweisende_instanz(data_all, zeit, typ, yscale = 20) 

#Abbildung 5.1.14.----
aufenthaltsdauer(data_all, zeit, typ)

#Abbildung 5.1.15.----
kurzaufenthalt_plot(data_all, zeit, typ)

#Abbildung 5.1.16.----
# HoNOS_ausgeschl_Faelle(data_all, zeit, typ)
# 
# #Abbildung 5.1.17.----
# BSCL_ausgeschl_Faelle(data_all, zeit, typ)


#Abbildung 5.1.XX Nebendiag ----
neben_diag(df, zeit, typ, yscale = 10) 

write.csv2(missing, file = paste0("Kliniktyp_", typ, "_Fehlend.csv"))



Zusatzinfo
#color definitions

#light blue:
rgb(154, 186, 202, maxColorValue = 255)
"#9ABACA"
#dark blue:
rgb(26, 91, 128, maxColorValue = 255)
"#1A5B80"

#dark green:
rgb(119, 147, 60, maxColorValue = 255)
"#77933C"
#medium green 1:
rgb(158, 169, 117, maxColorValue = 255)
"#9EA975"
#medium green 2:
rgb(175, 193, 143, maxColorValue = 255)
"#AFC18F"
#light green:
rgb(215, 228, 189, maxColorValue = 255)
"#D7E4BD"

#"schwarz"
rgb(38,38,38, maxColorValue = 255)
"#262626"
#"grau"
rgb(127,127,127, maxColorValue = 255)
"#7F7F7F"
#"beige"
rgb(148,138,84, maxColorValue = 255)
"#948A54"

rgb(183,222,232, maxColorValue = 255)
"#B7DEE8"
rgb(204,193,218, maxColorValue = 255)
"#CCC1DA"
rgb(230,185,184, maxColorValue = 255)
"#E6B9B8"
rgb(79,129,189, maxColorValue = 255)
"#4F81BD"

